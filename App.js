import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Image,
  View,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Text,
  ActivityIndicator,
  FlatList,
  Animated
} from 'react-native';

const images = [
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-8.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-10.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-9.jpg',
  'https://vienthammydiva.vn/wp-content/uploads/2022/09/gai-xinh-viet-nam-mac-do-thieu-vai-5.jpg'

]

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
const IMG_BG = 'https://cdn.pixabay.com/photo/2019/03/12/11/23/flower-4050535__480.jpg';
const HEIGHT_IMG = 80;
const ITEM_PADDING = 10;
const ITEM_MARGIN_BOTTOM = 20;
const ITEM_SIZE = HEIGHT_IMG + ITEM_PADDING * 2 + ITEM_MARGIN_BOTTOM;
const App = () => {

  const [data, setdata] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  const [ImgAcvive, setImgActive] = useState(0);
  const scrolly = React.useRef(new Animated.Value(0)).current;

  useEffect(() => {
    getListPhotos();
    return () => { }
  }, []);

  // lay du lieu tu API
  getListPhotos = () => {
    const apiURL = 'https://jsonplaceholder.typicode.com/photos';
    fetch(apiURL)
      .then((res) => res.json())
      .then((resJson) => {
        setdata(resJson)
      }).catch((error) => {
        console.log('Request API Error', error)
      }).finally(() => setisLoading(false))
  }

  // phân cách các ảnh và chuyển ảnh trong scollview
  onchange = (nativeEvent) => {
    if (nativeEvent) {
      const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
      if (slide != ImgAcvive) {
        setImgActive(slide);
      }
    }
  }

  // setUp item trong flatlist
  renderItem = ({ item, index }) => {

    // hieu ung thu nho item
    const scale = scrolly.interpolate({
      inputRange: [
        -1, 0,
        ITEM_SIZE * index,
        ITEM_SIZE * (index + 2)
      ],
      outputRange: [1, 1, 1, 0]
    })

    // hieu ung lam mo item
    const opacity = scrolly.interpolate({
      inputRange: [
        -1, 0,
        ITEM_SIZE * index,
        ITEM_SIZE * (index + .6)
      ],
      outputRange: [1, 1, 1, 0]
    })

    return (
      <Animated.View style={[
        styles.item,
        {
          transform: [{ scale }],
          opacity
        }
      ]}>
        <Image
          style={styles.img}
          source={{ uri: item.url }}
          resizeMode='contain'
        />

        <View style={styles.wrapText}>
          <Text style={styles.font}>{index + ' ' + item.title}</Text>
        </View>

      </Animated.View>
    )
  }

  return (

    <SafeAreaView style={styles.container} >

      <Image
        source={{ uri: IMG_BG }}
        style={StyleSheet.absoluteFillObject}
        blurRadius={20}
      />


      <View style={styles.wrap}>

        <ScrollView

          onScroll={({ nativeEvent }) => onchange(nativeEvent)}
          // ẩn thanh cuộn 
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          // indicatorStyle={'white'}
          // persistentScrollbar={true}
          style={styles.wrap} >
          {
            images.map((e, index) =>
              <Image
                key={e}
                resizeMode='stretch'
                style={styles.wrap}
                source={{ uri: e }} />
            )}

        </ScrollView>

        <View style={styles.wrapHeart}>
          {
            images.map((e, index) =>
              <Text
                key={e}
                style={ImgAcvive == index ? styles.HeartActive : styles.heart}
              >●</Text>
            )
          }
        </View>
      </View>

      <View style={{ flex: 2 }}>
        {
          isLoading ? <ActivityIndicator /> : (
            <Animated.FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={item => `key-${item.id}`}
              onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { y: scrolly } } }],
                { useNativeDriver: true }
              )}

            />
          )
        }
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1
  },

  wrap: {
    width: WIDTH,
    height: HEIGHT * 0.35
  },

  wrapHeart: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'center'
  },

  HeartActive: {
    margin: 2,
    color: '#31C6D4',

  },

  heart: {
    margin: 2,
    color: 'white',
  },

  img: {
    width: HEIGHT_IMG,
    height: 80,
  },

  wrapText: {
    flex: 1,
    marginLeft: 17,
    justifyContent: 'center',

  },

  item: {
    flexDirection: 'row',
    marginBottom: ITEM_MARGIN_BOTTOM,
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: '#181818',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.3,
    shadowRadius: 20,
    padding: ITEM_PADDING

  },

  font: {
    fontSize: 15,
    color: '#181818',
  }



});

export default App;
